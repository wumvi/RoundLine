'use strict';
/* global require */

var yaml = require('js-yaml');
var fs = require('fs');
var glob = require('glob');
var objectMerge = require('object-merge');


/**
 *
 * @param {number} oldIndex
 * @param {number} newIndex
 * @returns {Array}
 */
Array.prototype.move = function (oldIndex, newIndex) {
    if (newIndex >= this.length) {
        var k = newIndex - this.length;
        while ((k -= 1) + 1) {
            this.push(undefined);
        }
    }

    this.splice(newIndex, 0, this.splice(oldIndex, 1)[0]);
    return this;
};


/**
 *
 * @param {string} path Пуст к Yaml файлам
 * @param {string} name Назване списка файлов
 * @param {Object} rawData Сырые данные первого yaml файла
 * @param {string} jsSrcDir Директория с исходниками JS
 * @constructor
 */
function JsBuild(path, name, rawData, jsSrcDir) {
    /**
     * Тут будут хранится список JS файлов
     * @type {Array.<string>}
     * @private
     */
    this.jsFileList_ = [];

    /**
     * Имя списка файлов
     * @type {string}
     * @private
     */
    this.name_ = name;

    /**
     * Директория, где хранятся исходники JS файлов
     * @type {string}
     * @private
     */
    this.jsSrcDir_ = jsSrcDir;

    /**
     *
     * @type {Array.<string>}
     * @private
     */
    this.jsExternsList_ = [];

    this.init_(rawData);
}

/**
 * Инциализация объекта
 * @param {Object} rawData Сырые данные из первого yaml файла
 * @private
 */
JsBuild.prototype.init_ = function (rawData) {
    var that = this;
    this.parseData_(this.name_, rawData);

    this.jsFileList_ = this.jsFileList_.filter(function(elem, pos) {
        return that.jsFileList_.indexOf(elem) === pos;
    });
};

/**
 * Парсим файлы yaml
 *
 * @param {string} name Навание списка
 * @param {Object} rawData Сырые данные из yaml файла
 * @private
 */
JsBuild.prototype.parseData_ = function (name, rawData) {
    if (rawData[name].hasOwnProperty(JsBuild.INCLUDE_KEY)) {
        var includeList = rawData[name][JsBuild.INCLUDE_KEY];
        for (var includeNum in includeList) {
            if (!includeList.hasOwnProperty(includeNum)) {
                continue;
            }

            var includeName = includeList[includeNum];
            if (!rawData.hasOwnProperty(includeName)) {
                throw new Error('Include "' + includeName + '" not found');
            }

            this.parseData_(includeName, rawData);
        }
    }

    if (rawData[name].hasOwnProperty(JsBuild.EXTERNS_KEY)) {
        this.jsExternsList_ = this.jsExternsList_.concat(rawData[name][JsBuild.EXTERNS_KEY]);
    }

    if (rawData[name].hasOwnProperty(JsBuild.JS_LIST_KEY)) {
        this.jsFileList_ = this.jsFileList_.concat(rawData[name][JsBuild.JS_LIST_KEY]);
    }
};


/**
 * Получаем название списка
 * @return {string} Название списка
 */
JsBuild.prototype.getName = function () {
    return this.name_;
};


/**
 * Получаем список JS файлов
 * @return {Array.<string>} Список JS файлов
 */
JsBuild.prototype.getAllJsFileList = function () {
    var that = this;
    for(var num = 0; num < this.jsFileList_.length; num += 1) {
        var match = this.jsFileList_[num].match(/\[(\d+)\]/);
        if (match) {
            var pos = parseInt(match[1]);
            this.jsFileList_[num] = this.jsFileList_[num].replace(/\[(\d+)\]/, '');
            this.jsFileList_ = this.jsFileList_.move(num, match[1]);
        }
    }

    return this.jsFileList_.map(function(elem) {
        if (elem.indexOf('@vendor') !== -1) {
            return elem.replace('@vendor', 'vendor');
        } else if (elem.indexOf('@bower') !== -1) {
            return elem.replace('@bower', 'public/res/bower_package');
        }

        return  that.jsSrcDir_ + elem;
    });
};


/**
 *
 * @return {Array.<string>}
 */
JsBuild.prototype.getExternsList = function () {
    var that = this;
    return this.jsExternsList_.map(function(elem) {
        if (elem.indexOf('@vendor') !== -1) {
            return elem.replace('@vendor', 'vendor');
        } else if (elem.indexOf('@bower') !== -1) {
            return elem.replace('@bower', 'public/res/bower_package');
        }

        return  that.jsSrcDir_ + elem;
    });
};


/**
 * Ключ из yaml Файла. Какие блоки подключаются
 * @const {string}
 */
JsBuild.INCLUDE_KEY = 'include';

/**
 * Ключ из yaml Файла. Какие файлы подключаются
 * @const {string}
 */
JsBuild.JS_LIST_KEY = 'jsFile';


/**
 *
 * @const {string}
 */
JsBuild.EXTERNS_KEY = 'externs';


/**
 *
 * @param {string} path
 * @param {string} jsSrcDir
 * @param {string} fileName
 *
 * @return {Array.<JsBuild>}
 */
function makeJsBuildItemList(path, jsSrcDir, fileName) {
    var settingsBuffer = {};
    var settings = {};

    var fileYamlList = glob.sync(path + '/*.yaml', {
        'ignore': path + fileName + '.yaml'
    });
    for (var fileNum = 0; fileNum < fileYamlList.length; fileNum += 1) {
        settings = yaml.safeLoad(fs.readFileSync(fileYamlList[fileNum], 'utf8'));
        settingsBuffer = objectMerge(settingsBuffer, settings);
    }

    settings = yaml.safeLoad(fs.readFileSync(path + fileName + '.yaml', 'utf8'));
    settingsBuffer = objectMerge(settingsBuffer, settings);

    var firstBuildListRaw = [];
    for (var name in settings) {
        if (!settings.hasOwnProperty(name)) {
            continue;
        }

        firstBuildListRaw.push(new JsBuild(path, name, settingsBuffer, jsSrcDir));
    }

    return firstBuildListRaw;
}

module.exports = {
    'JsBuild': JsBuild,
    'fabric': {
        'makeJsBuildItemList': makeJsBuildItemList
    }
};
