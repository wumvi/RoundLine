'use strict';
/* global require, Promise */

var yaml = require('js-yaml');
var fs = require('fs');

var gulp = require('gulp');
var sass = require('gulp-sass');
var merge = require('merge-stream');
var md5File = require('md5-file');

var folderSassSrc = 'static/sass/';
var folderSassBin = './public/res/css/bin/';
var versionFile = './cache/version.json';
var path = require('path');


function buildJs(isDev, isUseCache) {
    isUseCache = isUseCache === undefined ? false : isUseCache;
    var argv = require('yargs').argv;
    var jsName = argv['js-name'];

    var compilerPackage = require('google-closure-compiler');
    var closureCompiler = compilerPackage.gulp();
    var gulpJsBuild = require('gulp-js-build');

    var jsMinDir = 'public/res/js/min/';
    var jsSrcDir = 'static/js/src/';
    var tasks = [];

    var versionData;
    try {
        versionData = JSON.parse(fs.readFileSync(versionFile, 'utf8'));
    } catch (ex) {
        versionData = {};
    }

    versionData.js = (new Date()).getTime();

    var hashYamlData = {'list': {}};
    var md5Filename = './cache/js-md5-' + (isDev ? 'dev' : 'prod') + '.yaml';
    var md5ImgData, fileNum;

    try {
        md5ImgData = yaml.safeLoad(fs.readFileSync(md5Filename, 'utf8'));
    } catch (ex) {
        md5ImgData = {list: {}};
    }

    var jsBuildItemList = gulpJsBuild.fabric.makeJsBuildItemList(
        'static/js/build/',
        jsSrcDir,
        'build'
    );

    if (jsName) {
        jsBuildItemList = jsBuildItemList.filter(function(item) {
            return item.getName() === jsName;
        });
    }

    for (var jsBuildItemNum = 0; jsBuildItemNum < jsBuildItemList.length; jsBuildItemNum += 1) {
        var outputFilename = jsBuildItemList[jsBuildItemNum].getName() + '.js';

        var fileList = jsBuildItemList[jsBuildItemNum].getAllJsFileList();

        for (fileNum = 0; fileNum < fileList.length; fileNum += 1) {
            try {
                fs.statSync(fileList[fileNum]);
            } catch (err) {
                console.error(fileList[fileNum]);
            }
        }

        // Проверка на изменение файлов, некий кеш быстрой компиляции
        var isContinueFlag = true;
        for (fileNum = 0; fileNum < fileList.length; fileNum += 1) {
            var filename = fileList[fileNum];
            var md5Hash = md5File.sync(filename);
            hashYamlData.list[md5Hash] = filename;

            if (md5ImgData.list[md5Hash] !== filename) {
                isContinueFlag = false;
            }
        }

        if (isContinueFlag && isUseCache) {
            continue;
        }

        console.log(outputFilename);

        var params = {
            compilation_level: 'ADVANCED_OPTIMIZATIONS',
            js_output_file: outputFilename,
            language_out: 'ECMASCRIPT5_STRICT',
            warning_level: 'VERBOSE',
            jscomp_warning: 'checkTypes',
            externs: jsBuildItemList[jsBuildItemNum].getExternsList(),
            generate_exports: null,
            js: 'node_modules/google-closure-library/closure/goog/base.js',
            output_wrapper: '(function() {%output%}).call(window);'
        };

        if (isDev) {
            params.formatting = 'PRETTY_PRINT';
            params.debug = true;
        }

        var tempTask = gulp.src(jsBuildItemList[jsBuildItemNum].getAllJsFileList())
            .pipe(closureCompiler(params))
            .pipe(gulp.dest(jsMinDir));

        tasks.push(tempTask);
    }

    fs.writeFileSync(md5Filename,
        '# This file created automatically by Gulp in "js-build"\r\n' + yaml.dump(hashYamlData));

    console.log('Task count: ' + tasks.length);
    fs.writeFileSync(versionFile, JSON.stringify(versionData));

    return merge(tasks);
}


/**
 *
 * @param {boolean} isDev
 * @param {string} type
 * @return {merge}
 */
function buildSass(isDev, type) {
    // var postcss      = require('gulp-postcss');
    // var sourcemaps   = require('gulp-sourcemaps');
    // var autoprefixer = require('autoprefixer');
    var replace = require('gulp-replace');

    var tasks = [];

    var versionData;
    try {
        versionData = JSON.parse(fs.readFileSync(versionFile, 'utf8'));
    } catch (ex) {
        versionData = {};
    }

    versionData.css = (new Date()).getTime();

    var buildProperties = yaml.safeLoad(fs.readFileSync('static/sass/' + type + '/build.yaml', 'utf8'));
    for (var num in buildProperties.list) {
        if (!buildProperties.list.hasOwnProperty(num)) {
            continue;
        }

        var folderItem = buildProperties.list[num];
        var mainSassFile = folderSassSrc + type + '/' + folderItem + '/main.scss';

        var sassOptions = {};
        if (!isDev) {
            sassOptions.outputStyle = 'compressed';
        }

        var tempTask = gulp.src(mainSassFile)
            //.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            //.pipe(sourcemaps.init())
            //.pipe(postcss([ autoprefixer({ browsers: ['last 2 versions', 'IE 9'] }) ]))
            //.pipe(sourcemaps.write('.'))
            .pipe(sass({
                includePaths: ['./public/res/bower_package/breakpoint-sass/stylesheets']
            }))
            .pipe(sass(sassOptions).on('error', sass.logError))
            // for IE hack  http://keithclark.co.uk/articles/moving-ie-specific-css-into-media-blocks/media-tests/
            .pipe(replace('IEOnly', 'screen\\0'))
            .pipe(gulp.dest(folderSassBin + type + '/' + folderItem));
        tasks.push(tempTask);
    }

    fs.writeFileSync(versionFile, JSON.stringify(versionData));

    return merge(tasks);
}


gulp.task('js-build-prod', buildJs.bind(null, false));
gulp.task('js-build-dev', buildJs.bind(null, true));
gulp.task('sass-site-prod', buildSass.bind(null, false, 'site'));
gulp.task('sass-site-dev', buildSass.bind(null, true, 'site'));


