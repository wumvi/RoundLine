'use strict';
/* global RoundLine */

var map = new RoundLine.MapBase();
map.addItem('are', 0, [2, 3, 6, 7]);
map.addItem('am', 2, [4]);
map.setItemPlaceLabels(['Anna', 'He', 'You', 'They', 'I', 'She', 'Mike and Olga', 'We']);
/* jshint ignore:start */
window['roundLineGameMapManager'].addMap(map);
/* jshint ignore:end */
