'use strict';
/* global RoundLine, jQuery */



/**
 * Event RoundLine.MainController.EVENT_WIN (id, map)
 *
 * @param {number|string} id
 * @param {!jQuery} $root
 * @param {number} placeCount
 * @param {RoundLine.MapBase} map
 * @constructor
 * @export
 */
RoundLine.MainController = function(id, $root, placeCount, map) {
    /**
     * @type {!jQuery}
     * @private
     */
    this.$root_ = $root;

    /**
     * @type {number|string}
     * @private
     */
    this.id_ = id;

    /**
     *
     * @type {RoundLine.RoundLineManager}
     * @private
     */
    this.lineManager_ = new RoundLine.RoundLineManager();
    /**
     *
     * @type {RoundLine.MapBase}
     * @private
     */
    this.map_ = map;

    /**
     * Количество мест
     * @type {number}
     * @private
     */
    this.placeCount_ = placeCount;

    /**
     * Список моделей-точек, куда можно поставить монетку
     * @type {Array.<RoundLine.ItemPlace>}
     * @private
     */
    this.placeList_ = [];

    /**
     * Модели монеток
     * @type {Array.<RoundLine.ItemPoint>}
     * @private
     */
    this.coinList_ = [];

    // TODO: Зам. на var style = window.getComputedStyle(this.canvasDomElement_, null).getPropertyValue('font-size');
    var $div = jQuery(document.createElement('DIV'));
    $div.addClass(RoundLine.MainController.CLASS_PLACE_ITEM);
    this.$root_.append($div);

    /**
     * Ширина поля
     * @type {number}
     * @private
     */
    this.rootSize_ = /** @type {number} */ (this.$root_.width());

    /**
     * Размер монетки
     * @type {number}
     * @private
     */
    this.coinWidth_ = /** @type {number} */ ($div.width());
    $div.remove();

    /**
     * Полный радиус поля
     * @type {number}
     * @private
     */
    this.radiusOffset_ = Math.round(this.rootSize_ / 2);

    var placeOffset = /** @type {number} */ (this.$root_.data('place-offset'));
    placeOffset *= this.rootSize_;

    /**
     *
     * @type {number}
     * @private
     */
    this.labelOffset_ = /** @type {number} */ (this.$root_.data('label-offset'));
    this.labelOffset_ = this.labelOffset_ * this.rootSize_;

    /**
     * Радиус
     * @type {number}
     * @private
     */
    this.radius_ = this.radiusOffset_ - placeOffset;

    /**
     *
     * @type {number}
     * @private
     */
    this.angleOffset_ = 360 / this.placeCount_;

    /**
     *
     * @type {boolean}
     * @private
     */
    this.isEndGame_ = false;

    /**
     *
     * @type {?RoundLine.ItemPoint}
     * @private
     */
    this.selectedCoin_ = null;

    /**
     * Список callback-ов
     * @type {RoundLine.EventListener}
     * @private
     */
    this.eventListener_ = new RoundLine.EventListener();

    this.init_();
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.init_ = function() {
    this.initItemPlace_();
    this.initItemLine_();
    this.initItemFigure_();

    for (var num = 0; num < this.placeCount_; num += 1) {
        var lineNum1 = num + 3 >= this.placeCount_ ? num + 3 - this.placeCount_ : num + 3;
        var lineNum2 = num + 5 >= this.placeCount_ ? num + 5 - this.placeCount_ : num + 5;

        var line1 = this.lineManager_.getLines(num);
        var line2 = this.lineManager_.getLines(lineNum2);

        var itemPlace = this.placeList_[num];
        itemPlace.addLine(line1);
        itemPlace.addLine(line2);

        line1.addItemPlace(itemPlace, this.placeList_[lineNum1]);
    }

    this.initEvent_();
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.initEvent_ = function() {
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.initItemLine_ = function() {
    for (var num = 0; num < this.placeCount_; num += 1) {
        this.lineManager_.add(new RoundLine.Line(num));
    }
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.initItemFigure_ = function() {
    var itemInfoList = this.map_.getItemPointList();
    for (var infoItemNum = 0; infoItemNum < itemInfoList.length; infoItemNum += 1) {
        var itemNum = itemInfoList[infoItemNum].getPosNum();
        var $div = jQuery(document.createElement('DIV'));
        $div.addClass(RoundLine.MainController.CLASS_FIGURE_ITEM);
        this.$root_.append($div);

        this.coinList_.push(new RoundLine.ItemPoint(
            this.placeList_[itemNum].getCoordinates().clone(),
            $div,
            this.onItemPointSelect_.bind(this),
            itemNum,
            itemInfoList[infoItemNum].getAnswerNumList()
        ));

        $div.html('<span class="rlg-coin__text">' + itemInfoList[infoItemNum].getText() + '</span>');

        this.placeList_[itemNum].setEnableFlag(false);
    }
};


/**
 *
 * @return {boolean}
 */
RoundLine.MainController.prototype.isEndGame = function() {
    for (var num = 0; num < this.coinList_.length; num += 1) {
        if (this.coinList_[num].getAnswerNumList().indexOf(this.coinList_[num].getItemNum()) === -1) {
            return false;
        }
    }

    return true;
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.initItemPlace_ = function() {
    var angle = 0;
    var halfItemWidth = this.coinWidth_ / 2;

    for (var num = 0; num < this.placeCount_; num += 1) {
        var xCos = Math.cos(angle * Math.PI / 180);
        var x = Math.round(xCos * this.radius_ + this.radiusOffset_ - halfItemWidth);

        var ySin = Math.sin(angle * Math.PI / 180);
        var y = Math.round(ySin * this.radius_ + this.radiusOffset_ - halfItemWidth);

        var div = document.createElement('DIV');
        jQuery(div).addClass(RoundLine.MainController.CLASS_PLACE_ITEM);
        this.$root_.append(div);

        var coordinates = new RoundLine.Coordinates(x, y);
        this.placeList_.push(new RoundLine.ItemPlace(
            coordinates,
            jQuery(div),
            this.onItemPlaceSelect_.bind(this),
            num
        ));

        var text = this.map_.getItemPlaceLabels()[num];
        var $div = jQuery(document.createElement('DIV'));
        $div.addClass(RoundLine.MainController.CLASS_PLACE_ITEM_TEXT);
        $div.text(text);
        this.$root_.append($div);

        var wordWidth = $div.outerWidth();
        console.log('wordWidth', wordWidth);
        var wordHeight = $div.outerHeight();

        var radius = this.radius_ + this.labelOffset_;

        x = xCos * radius + this.radiusOffset_;
        y = ySin * radius + this.radiusOffset_;

        if (90 < angle && angle < 270) {
            x -= wordWidth;
        } else if (angle === 90 || angle === 270) {
            x -= wordWidth / 2;
        }

        if (180 < angle && angle < 360) {
            y -= wordHeight;
        } else if (angle === 0 || angle === 180) {
            y -= wordHeight / 2;
        }

        $div.attr('style', '-ms-transform: translate(' + x + 'px, ' + y + 'px);' +
            '-webkit-transform: translate(' + x + 'px, ' + y + 'px);' +
            'transform: translate(' + x + 'px, ' + y + 'px);');

        angle += this.angleOffset_;
    }
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.mathPosPlace_ = function() {
    var angle = 0;
    var halfItemWidth = this.coinWidth_ / 2;

    for (var num = 0; num < this.placeCount_; num += 1) {
        var xCos = Math.cos(angle * Math.PI / 180);
        var x = Math.round(xCos * this.radius_ + this.radiusOffset_ - halfItemWidth);

        var ySin = Math.sin(angle * Math.PI / 180);
        var y = Math.round(ySin * this.radius_ + this.radiusOffset_ - halfItemWidth);

        var div = document.createElement('DIV');
        jQuery(div).addClass(RoundLine.MainController.CLASS_PLACE_ITEM);
        this.$root_.append(div);

        var coordinates = new RoundLine.Coordinates(x, y);
        this.placeList_.push(new RoundLine.ItemPlace(
            coordinates,
            jQuery(div),
            this.onItemPlaceSelect_.bind(this),
            num
        ));

        var text = this.map_.getItemPlaceLabels()[num];
        var $div = jQuery(document.createElement('DIV'));
        $div.addClass(RoundLine.MainController.CLASS_PLACE_ITEM_TEXT);
        $div.text(text);
        this.$root_.append($div);

        var wordWidth = $div.outerWidth();
        var wordHeight = $div.outerHeight();

        var radius = this.radius_ + this.labelOffset_;

        x = xCos * radius + this.radiusOffset_;
        y = ySin * radius + this.radiusOffset_;

        if (90 < angle && angle < 270) {
            x -= wordWidth;
        } else if (angle === 90 || angle === 270) {
            x -= wordWidth / 2;
        }

        if (180 < angle && angle < 360) {
            y -= wordHeight;
        } else if (angle === 0 || angle === 180) {
            y -= wordHeight / 2;
        }

        $div.attr('style', '-ms-transform: translate(' + x + 'px, ' + y + 'px);' +
            '-webkit-transform: translate(' + x + 'px, ' + y + 'px);' +
            'transform: translate(' + x + 'px, ' + y + 'px);');

        angle += this.angleOffset_;
    }
};


/**
 * @param {RoundLine.ItemPlace} itemPlace Выбранно место
 * @private
 */
RoundLine.MainController.prototype.onItemPlaceSelect_ = function(itemPlace) {
    if (this.isEndGame_) {
        return;
    }

    if (!this.selectedCoin_) {
        return;
    }

    if (!itemPlace.isEnable()) {
        return;
    }

    var flag = true;
    var lines = itemPlace.getLines();
    for (var num = 0; num < lines.length; num += 1) {
        if (!lines[num].isEnable()) {
            continue;
        }

        if (lines[num].getAnotherPlace(itemPlace) === this.placeList_[this.selectedCoin_.getItemNum()]) {
            flag = false;
            break;
        }
    }

    if (flag) {
        return;
    }

    // TODO: разобраться с выделение и обратным процессом ниже. Как-то странно, что мы убрали и заново поставили
    // Убираем выделение с монеты монетки
    this.setHighlightFlag(false, this.selectedCoin_);
    // Старое место делаем доступным
    this.placeList_[this.selectedCoin_.getItemNum()].setEnableFlag(true);
    // Текущее место делаем не доступным
    this.placeList_[itemPlace.getItemNum()].setEnableFlag(false);

    // Устанавливаем что текущий номер для монеты
    this.selectedCoin_.setItemNum(itemPlace.getItemNum());
    // Устанавливаем координаты на поле
    this.selectedCoin_.setPosition(itemPlace.getCoordinates());
    // Устанавливаем выделение с монеты монетки
    this.setHighlightFlag(true, this.selectedCoin_);

    setTimeout(this.onCheckDoneItem_.bind(this), 200);
    setTimeout(this.onTimeCheckWin_.bind(this), 300);
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.onCheckDoneItem_ = function() {
    /*
     var flag = this.selectedCoin_.getAnswerNumList().indexOf(this.selectedCoin_.getItemNum()) !== -1;
     this.selectedCoin_.setRightFlag(flag);
     */
};


/**
 *
 * @private
 */
RoundLine.MainController.prototype.onTimeCheckWin_ = function() {
    if (!this.isEndGame()) {
        return;
    }

    this.isEndGame_ = true;
    this.setHighlightFlag(false, this.selectedCoin_);
    this.selectedCoin_.setActiveFlag(false);
    this.selectedCoin_ = null;
    this.$root_.addClass('rlg-root--end-game');

    this.eventListener_.call(RoundLine.MainController.EVENT_WIN, [this.id_, this.map_]);
};


/**
 * При выделение монетки
 * @param {RoundLine.ItemPoint} coinItem Модель монетки
 * @private
 */
RoundLine.MainController.prototype.onItemPointSelect_ = function(coinItem) {
    if (this.isEndGame_) {
        return;
    }

    if (this.selectedCoin_) {
        this.selectedCoin_.setActiveFlag(false);
        this.setHighlightFlag(false, this.selectedCoin_);
    }

    this.selectedCoin_ = coinItem;
    this.selectedCoin_.setActiveFlag(true);

    this.setHighlightFlag(true, this.selectedCoin_);
};


/**
 *
 * @param {boolean} flag
 * @param {RoundLine.ItemPoint} pointItem
 */
RoundLine.MainController.prototype.setHighlightFlag = function(flag, pointItem) {
    var itemNum = pointItem.getItemNum();
    var lines = this.placeList_[itemNum].getLines();

    for (var num = 0; num < lines.length; num += 1) {
        if (!lines[num].isEnable()) {
            continue;
        }

        var crossItemPlace = lines[num].getAnotherPlace(this.placeList_[pointItem.getItemNum()]);
        if (!crossItemPlace.isEnable()) {
            continue;
        }

        crossItemPlace.setHighlightFlag(flag);
    }
};


/**
 *
 * @param {string} name
 * @param {Function} cb
 * @export
 */
RoundLine.MainController.prototype.addEventListener = function(name, cb) {
    this.eventListener_.add(name, cb);
};


/**
 *
 * @param {string} name
 * @private
 */
RoundLine.MainController.prototype.callEvent_ = function(name) {

};


/**
 * Класс текст точки-места
 * @const {string}
 */
RoundLine.MainController.CLASS_PLACE_ITEM_TEXT = 'rgl-place__text';


/**
 * Как точки-места
 * @const {string}
 */
RoundLine.MainController.CLASS_PLACE_ITEM = 'rlg-place';


/**
 * Класс монетки
 * @const {string}
 */
RoundLine.MainController.CLASS_FIGURE_ITEM = 'rlg-coin';


/**
 * Макс количество точек на карте
 * @const {number}
 * @export
 */
RoundLine.MainController.DEFAULT_ITEM_COUNT = 8;


/**
 * @const {string}
 * @export
 */
RoundLine.MainController.EVENT_WIN = 'event.win';
