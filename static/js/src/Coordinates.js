'use strict';
/* global RoundLine */



/**
 * Модель координат
 * @param {number} x Координата X
 * @param {number} y Координата Y
 * @constructor
 */
RoundLine.Coordinates = function(x, y) {
    /**
     * Координата X
     * @type {number}
     * @private
     */
    this.x_ = x;

    /**
     * Координата Y
     * @type {number}
     * @private
     */
    this.y_ = y;
};


/**
 * Получаем координату X
 * @return {number}
 */
RoundLine.Coordinates.prototype.getX = function() {
    return this.x_;
};


/**
 * Получаем координату Y
 * @return {number}
 */
RoundLine.Coordinates.prototype.getY = function() {
    return this.y_;
};


/**
 * Устанавливаем координату X
 * @param {number} x Координата X
 */
RoundLine.Coordinates.prototype.setX = function(x) {
    this.x_ = x;
};


/**
 * Устанавливаем координату Y
 * @param {number} y Координата Y
 */
RoundLine.Coordinates.prototype.setY = function(y) {
    this.y_ = y;
};


/**
 * Увеличиваем координату X на величину val
 * @param {number} val Смещение для X
 */
RoundLine.Coordinates.prototype.incX = function(val) {
    this.x_ += val;
};


/**
 * Увеличиваем координату Y на величину val
 * @param {number} val Смещение для Y
 */
RoundLine.Coordinates.prototype.incY = function(val) {
    this.y_ += val;
};


/**
 * Устанавливаем сразу координату X и Y
 * @param {number} x Координата X
 * @param {number} y Координата Y
 */
RoundLine.Coordinates.prototype.setXY = function(x, y) {
    this.x_ = x;
    this.y_ = y;
};


/**
 * Устанавливаем координаты
 * @param {RoundLine.Coordinates} coordinates Координаты
 */
RoundLine.Coordinates.prototype.set = function(coordinates) {
    this.x_ = coordinates.getX();
    this.y_ = coordinates.getY();
};


/**
 * Клонировании координаты
 * @return {RoundLine.Coordinates}
 */
RoundLine.Coordinates.prototype.clone = function() {
    return new RoundLine.Coordinates(this.getX(), this.getY());
};
