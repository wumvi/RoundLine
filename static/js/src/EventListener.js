'use strict';
/* global RoundLine, jQuery */



/**
 * Реализация событий
 * @constructor
 */
RoundLine.EventListener = function() {
    /**
     *
     * @type {Object}
     * @private
     */
    this.callEvent_ = {};
};


/**
 *
 * @param {string} name
 * @param {Function} cb
 */
RoundLine.EventListener.prototype.add = function(name, cb) {
    if (!this.callEvent_[name]) {
        this.callEvent_[name] = [];
    }

    this.callEvent_[name].push(cb);
};


/**
 *
 * @param {string} name
 * @param {Array} vars
 */
RoundLine.EventListener.prototype.call = function(name, vars) {
    if (this.callEvent_[name]) {
        var length = this.callEvent_[name].length;
        for (var i = 0; i < length; i += 1) {
            this.callEvent_[name][i].apply(this.callEvent_[name][i], vars);
        }
    }
};

