'use strict';
/* global RoundLine, jQuery */



/**
 * Логика точки
 * @param {RoundLine.Coordinates} coord Координаты места
 * @param {!jQuery} $item jQuery объект места
 * @param {Function} onItemPlaceSelect Callback при выделении
 * @param {number} num Позиция места
 * @constructor
 */
RoundLine.ItemPlace = function(coord, $item, onItemPlaceSelect, num) {
    /**
     * jQuery объект места
     * @type {!jQuery}
     * @private
     */
    this.$item_ = $item;

    /**
     * Позиция места
     * @type {number}
     * @private
     */
    this.itemNum_ = num;

    /**
     * Callback при выделении
     * @type {Function}
     * @private
     */
    this.onItemPlaceSelect_ = onItemPlaceSelect;

    /**
     * Координаты места
     * @type {RoundLine.Coordinates}
     * @private
     */
    this.coord_ = coord;

    /**
     * Массив линий, с которым связано место
     * @type {Array.<RoundLine.Line>}
     * @private
     */
    this.lineList_ = [];


    /**
     * Доступно ли место
     * @type {boolean}
     * @private
     */
    this.isEnable_ = true;


    this.init_();
};


/**
 * Инициализация
 * @private
 */
RoundLine.ItemPlace.prototype.init_ = function() {
    this.moveItem_(this.coord_.getX(), this.coord_.getY());
    this.initEvent_();
};


/**
 * Инициализация событий
 * @private
 */
RoundLine.ItemPlace.prototype.initEvent_ = function() {
    this.$item_.click(this.onMouseClick_.bind(this));
};


/**
 * Номер точки
 * @return {number} Номер места
 */
RoundLine.ItemPlace.prototype.getItemNum = function() {
    return this.itemNum_;
};


/**
 * Обработка клика по месту
 * @return {boolean} PreventEventDefault
 * @private
 */
RoundLine.ItemPlace.prototype.onMouseClick_ = function() {
    this.onItemPlaceSelect_(this);
    return false;
};


/**
 * Получаем координаты места
 * @return {RoundLine.Coordinates} Координаты места
 */
RoundLine.ItemPlace.prototype.getCoordinates = function() {
    return this.coord_;
};


/**
 * Двигаем место в новую позицию
 * @param {number} x Координата X
 * @param {number} y Координата Y
 */
RoundLine.ItemPlace.prototype.move = function(x, y) {
    this.moveItem_(x, y);
};


/**
 * Добавляем связанную линию с местом
 * @param {RoundLine.Line} line Модель линии
 */
RoundLine.ItemPlace.prototype.addLine = function(line) {
    this.lineList_.push(line);
};


/**
 * Получаем связанные линии
 * @return {Array.<RoundLine.Line>} Массив связанных линий
 */
RoundLine.ItemPlace.prototype.getLines = function() {
    return this.lineList_;
};


/**
 * Двигаем место
 * @param {number} x Координата X
 * @param {number} y Координата Y
 * @private
 */
RoundLine.ItemPlace.prototype.moveItem_ = function(x, y) {
    this.$item_.attr('style', '-ms-transform: translate(' + x + 'px, ' + y + 'px);' +
        '-webkit-transform: translate(' + x + 'px, ' + y + 'px);' +
        'transform: translate(' + x + 'px, ' + y + 'px);');
};


/**
 * Доступна ли место
 * @return {boolean} true - да, false - нет
 */
RoundLine.ItemPlace.prototype.isEnable = function() {
    return this.isEnable_;
};


/**
 * Устанавливаем доступность места
 * @param {boolean} flag true - да, false - нет
 */
RoundLine.ItemPlace.prototype.setEnableFlag = function(flag) {
    this.isEnable_ = flag;
};


/**
 * Устанавливаем подсветку места
 * @param {boolean} flag true - подсвечена, false - нет
 */
RoundLine.ItemPlace.prototype.setHighlightFlag = function(flag) {
    this.$item_.toggleClass(RoundLine.ItemPlace.CLASS_PLACE_ACTIVE, flag);
};


/**
 * CSS для подсветки места
 * @const {string}
 */
RoundLine.ItemPlace.CLASS_PLACE_ACTIVE = 'rlg-place--active';



