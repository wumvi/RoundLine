'use strict';
/* global RoundLine, jQuery */



/**
 * @param {RoundLine.Coordinates} coord
 * @param {!jQuery} $item
 * @param {Function} onItemPointSelect
 * @param {number} itemNum
 * @param {Array.<number>} answerNumList
 * @constructor
 */
RoundLine.ItemPoint = function(coord, $item, onItemPointSelect, itemNum, answerNumList) {
    /**
     * @type {!jQuery}
     * @private
     */
    this.$item_ = $item;

    /**
     *
     * @type {Function}
     * @private
     */
    this.onItemPointSelect_ = onItemPointSelect;

    /**
     *
     * @type {RoundLine.Coordinates}
     * @private
     */
    this.coord_ = coord;

    /**
     *
     * @type {number}
     * @private
     */
    this.itemNum_ = itemNum;

    /**
     *
     * @type {Array.<number>}
     * @private
     */
    this.answerNumList_ = answerNumList;

    this.init_();
};


/**
 *
 * @private
 */
RoundLine.ItemPoint.prototype.init_ = function() {
    this.moveItem_(this.coord_.getX(), this.coord_.getY());

    this.initEvent_();
};


/**
 *
 * @private
 */
RoundLine.ItemPoint.prototype.initEvent_ = function() {
    this.$item_.click(this.onMouseClick_.bind(this));
};


/**
 * @param {boolean} flag
 */
RoundLine.ItemPoint.prototype.setActiveFlag = function(flag) {
    this.$item_.toggleClass(RoundLine.ItemPoint.CLASS_COIN_SELECTED, flag);
};


/**
 * @param {boolean} flag
 */
RoundLine.ItemPoint.prototype.setRightFlag = function(flag) {
    this.$item_.toggleClass(RoundLine.ItemPoint.CLASS_COIN_RIGHT, flag);
};


/**
 * @return {boolean}
 * @private
 */
RoundLine.ItemPoint.prototype.onMouseClick_ = function() {
    this.onItemPointSelect_(this);
    return false;
};


/**
 * @return {RoundLine.Coordinates}
 */
RoundLine.ItemPoint.prototype.getCoordinates = function() {
    return this.coord_;
};


/**
 * @return {number}
 */
RoundLine.ItemPoint.prototype.getItemNum = function() {
    return this.itemNum_;
};


/**
 * @param {number} num
 */
RoundLine.ItemPoint.prototype.setItemNum = function(num) {
    this.itemNum_ = num;
};


/**
 *
 * @param {RoundLine.Coordinates} coord
 */
RoundLine.ItemPoint.prototype.setPosition = function(coord) {
    this.coord_ = coord;
    this.moveItem_(this.coord_.getX(), this.coord_.getY());
};


/**
 *
 * @param {number} x
 * @param {number} y
 * @private
 */
RoundLine.ItemPoint.prototype.moveItem_ = function(x, y) {
    this.$item_.attr('style', '-ms-transform: translate(' + x + 'px, ' + y + 'px);' +
        '-webkit-transform: translate(' + x + 'px, ' + y + 'px);' +
        'transform: translate(' + x + 'px, ' + y + 'px);');
};


/**
 *
 * @return {Array.<number>}
 */
RoundLine.ItemPoint.prototype.getAnswerNumList = function() {
    return this.answerNumList_;
};


/**
 *
 * @const {string}
 * @export
 */
RoundLine.ItemPoint.CLASS_COIN_SELECTED = 'rlg-coin--active';


/**
 *
 * @const {string}
 * @export
 */
RoundLine.ItemPoint.CLASS_COIN_RIGHT = 'rlg-coin--right';
