'use strict';
/* global RoundLine, jQuery */



/**
 * Модель линии
 * @param {number} num Номер линии
 * @constructor
 */
RoundLine.Line = function(num) {
    /**
     * С каким местом-1 связана линия
     * @type {?RoundLine.ItemPlace}
     * @private
     */
    this.itemPlace1_ = null;


    /**
     * С каким местом-2 связана линия
     * @type {?RoundLine.ItemPlace}
     * @private
     */
    this.itemPlace2_ = null;

    /**
     * Доступна ли линия
     * @type {boolean}
     * @private
     */
    this.isEnable_ = true;
};


/**
 * Получить другое место
 * @param {RoundLine.ItemPlace} itemPlace Одно из мест
 * @return {RoundLine.ItemPlace} Другая противоположное место
 */
RoundLine.Line.prototype.getAnotherPlace = function(itemPlace) {
    if (itemPlace !== this.itemPlace2_) {
        return this.itemPlace2_;
    }

    return this.itemPlace1_;
};


/**
 * Добавляем место
 * @param {RoundLine.ItemPlace} itemPlace1 Место 1
 * @param {RoundLine.ItemPlace} itemPlace2 Место 2
 */
RoundLine.Line.prototype.addItemPlace = function(itemPlace1, itemPlace2) {
    this.itemPlace1_ = itemPlace1;
    this.itemPlace2_ = itemPlace2;
};


/**
 * Доступность линии
 * @return {boolean} true - да, false - нет
 */
RoundLine.Line.prototype.isEnable = function() {
    return this.isEnable_;
};
