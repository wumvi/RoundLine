'use strict';
/* global RoundLine, jQuery */



/**
 * Менеджер линий
 * @constructor
 */
RoundLine.RoundLineManager = function() {
    /**
     * Массив линий
     * @type {Array.<RoundLine.Line>}
     * @private
     */
    this.line_ = [];
};


/**
 * Добавить линию
 * @param {RoundLine.Line} line Модель линии
 */
RoundLine.RoundLineManager.prototype.add = function(line) {
    this.line_.push(line);
};


/**
 * Получить линию
 * @param {number} pos Номер линии
 * @return {RoundLine.Line} Модель линии
 */
RoundLine.RoundLineManager.prototype.getLines = function(pos) {
    return this.line_[pos];
};
