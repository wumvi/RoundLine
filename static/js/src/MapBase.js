'use strict';
/* global RoundLine */



/**
 * Модель карты
 * @constructor
 * @export
 */
RoundLine.MapBase = function() {
    /**
     * Информация о точках
     * @type {Array.<RoundLine.MapWordInfo>}
     * @private
     */
    this.itemPointInfoist_ = [];

    /**
     * Подписи объектов
     * @type {Array.<string>}
     * @private
     */
    this.labelList_ = [];
};


/**
 * Добавляем точку в игру
 * @param {string} text Текст вопроса
 * @param {number} posNum Начальная позиция
 * @param {Array.<number>} answerPos Позиции ответов
 * @export
 */
RoundLine.MapBase.prototype.addItem = function(text, posNum, answerPos) {
    this.itemPointInfoist_.push(new RoundLine.MapWordInfo(text, posNum, answerPos));
};


/**
 * Получаем точки
 * @return {Array.<RoundLine.MapWordInfo>} Массив точек слов
 * @export
 */
RoundLine.MapBase.prototype.getItemPointList = function() {
    return this.itemPointInfoist_;
};


/**
 * Выставляем подписи объектов
 * @param {Array.<string>} labelList Подписи объектов
 * @export
 */
RoundLine.MapBase.prototype.setItemPlaceLabels = function(labelList) {
    this.labelList_ = labelList;
};


/**
 * Получаем подписи объектов
 * @return {Array.<string>} Подписи объектов
 * @export
 */
RoundLine.MapBase.prototype.getItemPlaceLabels = function() {
    return this.labelList_;
};
