'use strict';
/* global RoundLine, jQuery */



/**
 * @constructor
 * @export
 */
RoundLine.MapManager = function() {
    /**
     *
     * @type {Array.<RoundLine.MapBase>}
     * @private
     */
    this.mapList_ = [];
};


/**
 *
 * @param {RoundLine.MapBase} map
 * @export
 */
RoundLine.MapManager.prototype.addMap = function(map) {
    this.mapList_.push(map);
};


/**
 *
 * @param {number} num
 * @return {RoundLine.MapBase}
 * @export
 */
RoundLine.MapManager.prototype.getMap = function(num) {
    return this.mapList_[num];
};


/* jshint ignore:start */
if (!window['roundLineGameMapManager']) {
    window['roundLineGameMapManager'] = new RoundLine.MapManager();
}
/* jshint ignore:end */
