'use strict';
/* global RoundLine, jQuery */



/**
 * @param {string} text
 * @param {number} posNum
 * @param {Array.<number>} answerNumList
 * @constructor
 * @export
 */
RoundLine.MapWordInfo = function(text, posNum, answerNumList) {

    /**
     *
     * @type {string}
     * @private
     */
    this.text_ = text;

    /**
     *
     * @type {number}
     * @private
     */
    this.posNum_ = posNum;

    /**
     *
     * @type {Array.<number>}
     * @private
     */
    this.answerNumList_ = answerNumList;
};


/**
 *
 * @return {string}
 */
RoundLine.MapWordInfo.prototype.getText = function() {
    return this.text_;
};


/**
 *
 * @return {number}
 */
RoundLine.MapWordInfo.prototype.getPosNum = function() {
    return this.posNum_;
};


/**
 *
 * @return {Array.<number>}
 */
RoundLine.MapWordInfo.prototype.getAnswerNumList = function() {
    return this.answerNumList_;
};

